#!/bin/bash

MODELS=("model_0129739")

for MODEL in "${MODELS[@]}"
do
    MODEL_PATH="/gdrnpp_bop2022/output/output_world_occ_01/gdrn/fruitbin/convnext_a6_AugCosyAAEGray_BG05_mlL1_DMask_amodalClipBox_classAware_fruitbin/$MODEL.pth"

    /gdrnpp_bop2022/core/gdrn_modeling/test_gdrn.sh /gdrnpp_bop2022/configs/gdrn/fruitbin/convnext_a6_AugCosyAAEGray_BG05_mlL1_DMask_amodalClipBox_classAware_fruitbin.py 0 $MODEL_PATH

    LOGFILE="/gdrnpp_bop2022/output/output_world_occ_01/gdrn/fruitbin/convnext_a6_AugCosyAAEGray_BG05_mlL1_DMask_amodalClipBox_classAware_fruitbin/$MODEL.log"
    PRED_PATH="/gdrnpp_bop2022/output/gdrn/fruitbin/convnext_a6_AugCosyAAEGray_BG05_mlL1_DMask_amodalClipBox_classAware_fruitbin/inference_$MODEL/fruitbin_test/convnext-a6-AugCosyAAEGray-BG05-mlL1-DMask-amodalClipBox-classAware-fruitbin-test-iter0_fruitbin-test.csv"

    declare -A classes=(
        ["apple2"]="True"
        ["apricot"]="True"
        ["banana1"]="False"
        ["kiwi1"]="True"
        ["lemon2"]="True"
        ["orange2"]="True"
        ["peach1"]="True"
        ["pear2"]="False"
    )

    for CLASS in "${!classes[@]}"
    do
        echo "Evaluating $CLASS" | tee -a $LOGFILE
        python /gdrnpp_bop2022/core/gdrn_modeling/tools/fruitbin/eval_pose.py --path_data=/gdrnpp_bop2022/datasets/BOP_DATASETS/fruitbin/ --pred_path=$PRED_PATH --class_name=$CLASS --symmetry=${classes[$CLASS]} | tee -a $LOGFILE
    done

done
