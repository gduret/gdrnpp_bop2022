import json
import os

bbox_path = f'/home/Downloads/Datasets/BOP_format/test'
output = "/home/Downloads/Datasets/BOP_format/test/test_bboxes/"
os.makedirs(output, exist_ok=True)
output_file = f"{output}gt_all_fruits_fruitbin_pbr_fruitbin_bop_test.json"

directories = [d for d in os.listdir(bbox_path) if os.path.isdir(os.path.join(bbox_path, d))]
directories = sorted(directories)

updated_data = {}
obj_id = 1

for scene_id, folder in enumerate(directories):
    file_path = os.path.join(bbox_path, folder, "scene_gt_info.json")
    if os.path.exists(file_path):
        with open(file_path, 'r') as file:
            bbox_data = json.load(file)

        for key, values in bbox_data.items():
            updated_key = f"{scene_id}/{key}"
            updated_data[updated_key] = [{"bbox": val["bbox_visib"], "obj_id": obj_id} for val in values]
        obj_id += 1

with open(output_file, 'w') as outfile:
    json.dump(updated_data, outfile, indent=2)

print(f"The file was successfully created: {output_file}")
