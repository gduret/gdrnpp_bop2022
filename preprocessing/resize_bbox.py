import os


def adjust_bbox(input_dir, output_dir):
    for filename in os.listdir(input_dir):
        if filename.endswith(".txt"):
            input_file_path = os.path.join(input_dir, filename)
            output_file_path = os.path.join(output_dir, filename)

            with open(input_file_path, 'r') as f:
                coords = f.readline().split()

                x1, y1, x2, y2 = map(float, coords)

                x1_new = x1 * (1 / 2)
                y1_new = y1 * (2 / 3)
                x2_new = x2 * (1 / 2)
                y2_new = y2 * (2 / 3)

                with open(output_file_path, 'w') as out_f:
                    out_f.write(f"{x1_new} {y1_new} {x2_new} {y2_new}\n")


input_dir = '/home/Downloads/Datasets/FruitBin/banana1/Bbox'
output_dir = '/home/Downloads/Datasets/FruitBin/banana1/Bbox_resized'
adjust_bbox(input_dir, output_dir)

