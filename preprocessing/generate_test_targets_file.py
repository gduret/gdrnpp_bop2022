import json
import os

path = '/home/Downloads/Datasets/BOP_format/test'

selected_images = []

directories = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
directories = sorted(directories)

for scene_id, fruit_folder in enumerate(directories):
    print(scene_id)
    fruit_path = os.path.join(path, fruit_folder)
    print(fruit_path)
    if os.path.isdir(fruit_path):
        scene_gt_path = os.path.join(fruit_path, 'scene_gt.json')
        if os.path.isfile(scene_gt_path):
            with open(scene_gt_path, 'r') as file:
                scene_gt_data = json.load(file)

                for im_id, objects in scene_gt_data.items():
                    for obj in objects:
                        selected_images.append({
                            "im_id": int(im_id),
                            "inst_count": 1,
                            "obj_id": obj["obj_id"],
                            "scene_id": scene_id
                        })

output_path = os.path.join('/home/Downloads/Datasets/BOP_format/test_targets_bop19.json')
with open(output_path, 'w') as outfile:
    json.dump(selected_images, outfile, indent=4)

print(f'File "test_targets_bop19.json" has been created with {len(selected_images)} entries.')
