import os
import shutil
import numpy as np
import json
import re
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="Preprocess data for different scenarios and categories.")
    parser.add_argument('--src_directory', type=str, required=True, help='Source directory path.')
    parser.add_argument('--dst_directory', type=str, required=True, help='Destination directory path.')
    parser.add_argument('--scenario', type=str, default='_world_occ_07.txt', help='Scenario file.')
    parser.add_argument('--categories', type=str, nargs='+', default=['train', 'test'], help='Data categories.')
    return parser.parse_args()


def splitting(splitting_path, scenario, category, folder):
    file_ids = []

    fruit = re.sub(r'\d+$', '', folder)

    split_file = os.path.join(splitting_path, f"{category}_{fruit}{scenario}")
    if os.path.exists(split_file):
        with open(split_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip().split('.')[0]
                if line:
                    file_ids.append(line)

    return file_ids


def copy_specific_folder(dst_directory, src_directory, old_folder, new_folder, category, scenario):
    os.makedirs(dst_directory, exist_ok=True)

    folders_in_src = [d for d in os.listdir(src_directory) if os.path.isdir(os.path.join(src_directory, d))]

    for folder in folders_in_src:
        if category == "train":
            new_folder_path = os.path.join(dst_directory, "train_pbr", folder)
        else:
            new_folder_path = os.path.join(dst_directory, category, folder)

        os.makedirs(new_folder_path, exist_ok=True)

        splitting_path = os.path.join(src_directory, folder, "Splitting")
        file_names_to_copy = splitting(splitting_path, scenario, category, folder)

        src_subfolder = os.path.join(src_directory, folder)
        dst_subfolder = os.path.join(new_folder_path, new_folder)
        os.makedirs(dst_subfolder, exist_ok=True)

        for file_name in file_names_to_copy:
            src_file_path = os.path.join(src_subfolder, old_folder, file_name + ".png")
            if os.path.exists(src_file_path):
                dst_file_path = os.path.join(dst_subfolder, file_name + ".png")
                shutil.copy(src_file_path, dst_file_path)

                new_file_name = f"{int(file_name):06}.png"
                new_dst_file_path = os.path.join(dst_subfolder, new_file_name)
                os.rename(dst_file_path, new_dst_file_path)

        if not os.path.exists(os.path.join(new_folder_path, "scene_gt.json")):
            process_directory(new_folder_path, src_directory, file_names_to_copy, folder)


def process_directory(new_folder_path, src_subfolder, file_names_to_copy, folder):
    matrix = [543.25272224, 0., 320.25, 0., 724.33696299, 240.33333333, 0., 0., 1.]

    obj_ids = {"apple2": 1, "apricot": 2, "banana1": 3, "kiwi1": 4, "lemon2": 5, "orange2": 6, "peach1": 7, "pear2": 8}

    pose_transformed_path = os.path.join(src_subfolder, folder, "Pose_transformed")

    all_objects_data = {}
    if os.path.exists(pose_transformed_path):
        for filename in os.listdir(pose_transformed_path):
            if filename.endswith('.npy'):
                id_file = filename.split(".")[0]
                if id_file not in file_names_to_copy:
                    continue
                file_path = os.path.join(pose_transformed_path, filename)
                data = np.load(file_path)
                cam_R_m2c = np.concatenate((data[0, :3], data[1, :3], data[2, :3]), axis=0).tolist()
                cam_t_m2c = np.array(
                    (float(data[0, 3]) * 1000.0, float(data[1, 3]) * 1000.0, float(data[2, 3]) * 1000.0)).tolist()
                all_objects_data[id_file] = {
                    "cam_R_m2c": cam_R_m2c,
                    "cam_t_m2c": cam_t_m2c,
                    "obj_id": obj_ids[folder]
                }

    path_to_scene_gt_json = os.path.join(new_folder_path, "scene_gt.json")

    with open(path_to_scene_gt_json, 'w') as json_file:
        json_file.write('{\n')
        total_items = len(all_objects_data)
        for idx, (id_file, object_data) in enumerate(all_objects_data.items()):
            object_data_list = [object_data]
            object_data_str = json.dumps(object_data_list, separators=(',', ':'))
            json_file.write(f'  "{id_file}": {object_data_str}')
            if idx < total_items - 1:
                json_file.write(',\n')
        json_file.write('\n}')

    bbox_path = os.path.join(src_subfolder, folder, "Bbox_resized")

    all_bbox_data = {}
    for filename in os.listdir(bbox_path):
        if filename.endswith('.txt'):
            id_file = filename.split(".")[0]
            if id_file not in file_names_to_copy:
                continue
            file_path = os.path.join(bbox_path, filename)
            bbox_data = np.loadtxt(file_path).reshape(-1)
            top_left_x, top_left_y, bottom_right_x, bottom_right_y = bbox_data
            width = bottom_right_x - top_left_x
            height = bottom_right_y - top_left_y
            all_bbox_data[id_file] = {
                "bbox_visib": [int(top_left_x), int(top_left_y), int(width), int(height)]
            }

    path_to_scene_gt_info_json = os.path.join(new_folder_path, "scene_gt_info.json")

    with open(path_to_scene_gt_info_json, 'w') as json_file:
        json_file.write('{\n')
        total_count = len(all_bbox_data)
        for idx, (id_file, bbox_info) in enumerate(all_bbox_data.items()):
            bbox_info_list = [bbox_info]
            json_file.write(f'  "{id_file}": {json.dumps(bbox_info_list, separators=(",", ":"))}')
            if idx < total_count - 1:
                json_file.write(',\n')
            else:
                json_file.write('\n')
        json_file.write('}')

    all_camera_data = {str(id_file): {"cam_K": matrix, "depth_scale": 0.1} for id_file in all_objects_data.keys()}
    path_to_scene_camera_json = os.path.join(new_folder_path, "scene_camera.json")
    with open(path_to_scene_camera_json, 'w') as json_file:
        json_file.write('{\n')
        for idx, (id_file, camera_info) in enumerate(all_camera_data.items()):
            camera_info_str = json.dumps(camera_info, separators=(',', ':'))
            json_file.write(f'  "{id_file}": {camera_info_str}')
            if idx < len(all_camera_data) - 1:
                json_file.write(',\n')
            else:
                json_file.write('\n')
        json_file.write('}')

    return {"objects_data": all_objects_data, "bbox_data": all_bbox_data, "camera_data": all_camera_data}


if __name__ == "__main__":
    args = parse_args()
    src_directory = args.src_directory
    dst_directory = args.dst_directory
    scenario = args.scenario
    categories = args.categories

    for category in categories:
        copy_specific_folder(dst_directory, src_directory, "RGB_resized", "rgb", category, scenario)
        print(f"RGB files for the {category}ing dataset have been copied")
        print(f".json gt files for the {category}ing dataset have been copied")
        copy_specific_folder(dst_directory, src_directory, "Instance_Mask_resized", "mask_visib", category, scenario)
        print(f"Masks for the {category}ing dataset have been copied")
        copy_specific_folder(dst_directory, src_directory, "Depth_resized", "depth", category, scenario)
        print(f"Depth files for the {category}ing dataset have been copied")
        print(f"The {category}ing dataset is ready\n")
