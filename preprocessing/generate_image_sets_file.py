import re
import os


def convert_numbers(input_file, output_file, scene):
    os.makedirs(output_file, exist_ok=True)
    with open(input_file, 'r') as infile, open(output_file + "/keyframe.txt", 'a') as outfile:
        for line in infile:
            number = int(line.strip().split('.')[0])
            outfile.write(f"000{scene}/{number}\n")

"""
Possible scenarios:
    _world_occ_07.txt, _world_occ_05.txt, _world_occ_03.txt, _world_occ_01.txt, 
    _camera_occ_07.txt, _camera_occ_05.txt, _camera_occ_03.txt, _camera_occ_01.txt
"""

scenario = "_world_occ_07.txt"
scenario1 = scenario.split('.')[0]
objects = ["apple2", "apricot", "banana1", "kiwi1", "lemon2", "orange2", "peach1", "pear2"]
cleaned_objects = [re.sub(r'\d$', '', item) for item in objects]
for i, object in enumerate(objects):
    fruit = objects[i]
    scene = i
    convert_numbers(f'/home/Downloads/Datasets/FruitBin/{object}/Splitting/splitting_occ_01_worlds/test_{fruit}{scenario}',
                f'/home/Downloads/Datasets/BOP_format/image_sets', scene)
    print(f"Data for scene {scene} ({object}) is ready")

